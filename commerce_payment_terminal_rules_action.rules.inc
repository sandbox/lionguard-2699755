<?php

/**
 * @file
 * Provide rule action for creating payment transaction.
 */

/**
 * Implement hook_rules_action_info().
 */
function commerce_payment_terminal_rules_action_rules_action_info() {

  $actions = array(
    'commerce_payment_terminal_rules_action_transaction_create' => array(
      'label' => t('Commerce transaction create'),
      'group' => t('Commerce Payment'),
      'parameter' => array(
        'param_payment_method' => array(
          'type' => 'text',
          'label' => t('Payment settings'),
          'options list' => 'commerce_payment_method_options_list',
          'restriction' => 'input',
        ),
        'status' => array(
          'type' => 'text',
          'label' => t('Payment status'),
          'options list' => '_get_payment_options_list',
          'restriction' => 'input',
        ),
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('The user who created the transaction.'),
        ),
        'order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The order the transaction belongs to.'),
        ),
        'amount' => array(
          'type' => 'text',
          'label' => t('Amount'),
          'description' => t('Add amount'),
          'restriction' => 'input',
        ),
      ),
    ),
  );

  return $actions;
}

/**
 * Rules callback.
 *
 * @param string $param_payment_method
 * @param string $status
 * @param object $user
 * @param object $order
 * @param integer $amount
 */
function commerce_payment_terminal_rules_action_transaction_create($param_payment_method, $status, $user, $order, $amount) {
  $transaction = entity_get_controller('commerce_payment_transaction')->create(array(
    'payment_method' => $param_payment_method,
    'order_id' => $order->order_id,
    'status' => $status,
    'amount' => $amount,
    'uid' => $user->uid,
  ));

  commerce_payment_transaction_save($transaction);
}

/**
 * Provide options for rule action select list.
 *
 * @return Array|bool
 */
function _get_payment_options_list() {
  foreach (commerce_payment_transaction_statuses() as $key => $value) {
    $options[$key] = $value['title'];
  }
  return !empty($options) ? $options : FALSE;
}